# On first checkout
## Initialize submodules like the hugo theme
```
git submodule init && git submodule update
```
## Updating a submodule
### Updating all modules in this repo
```
git submodule foreach git pull
```

### Updating the learn theme
```
cd themes/hugo-learn-theme
git pull
```


# Some commands for Hugo
To run it with live reload locally just use:
```
hugo server
```

# Documentation about the learn theme we use
The official documentation can be found at https://matcornic.github.io/hugo-learn-doc and the source of it which is a sample on https://github.com/matcornic/hugo-learn-doc .

## Creating new chapters
```
hugo new --kind chapter <insert.chapter.name>/index.md
```

## Creating a content page inside a chapter
```
hugo new <insert.chapter.name>/<insert.content.name>.md
```
